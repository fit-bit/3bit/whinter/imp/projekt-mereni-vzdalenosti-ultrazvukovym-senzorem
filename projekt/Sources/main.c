/*
 * main.c
 * Autor: Mat�j Kudera (xkuder04)
 * Projekt IMP - M��en� vzd�lenosti ultrazvukov�m senzorem.
 * Program bere informace z ultrazvukov�ho senzoru a zobrazuje nam��enou vzd�lenost v CM na segmentov�m display.
 */

#include <math.h>
#include "MK60D10.h"

/*
 * Zapojen�
 *
 * Senzor:
 * VCC - 2
 * GND - 49
 * Echo - 35 (PTA27)
 * Trig - 37 (PTA26)

 * Display (v�znam na display) na Konektor:
 * D1 (SEG.E) - K4
 * D2 (SEG.D) - K5
 * D3 (SEG.DP)- K6
 * D4 (SEG.C) - K7
 * D5 (SEG.G) - K8
 * D6 (DIG.4) - K11
 * D7 (SEG.B) - K1
 * D8 (DIG.3) - K10
 * D9 (DIG.2) - K9
 * D10 (SEG.F) - K2
 * D11 (SEG.A) - K3
 * D12 (DIG.1) - K12

 Konektor na piny desky (+ ozna�en� pinu v procesoru):
 * 1 - 17 (MCU_I2C0_SCL), PTD8
 * 2 - 18 (MCU_I2C0_SDA), PTD9
 * 3 - 19 (MCU_SPI2_CLK), na procesoru napojeno na SPI2_SCK, PTD12
 * 4 - 20 (MCU_SPI2_MOSI), na procesoru napojeno na SPI2_SOUT, PTD13
 * 5 - 21 (MCU_SPI2_CS1), na procesoru napojeno na SPI2_PCS1, PTD15
 * 6 - 22 (MCU_SPI2_MISO), na procesoru napojeno na SPI2_SIN. PTD14
 * 7 - 23 (PTA8)
 * 8 - 24 (PTA10)
 * 9 - 25 (PTA6)
 * 10 - 26 (PTA11)
 * 11 - 27 (PTA7)
 * 12 - 28 (PTA9)
 *
 */

// Definice masek
#define TRIG_PIN 0x4000000
#define ECHO_PIN 0x8000000

#define OUTPUT_PINS_A 0x4000FC0
#define OUTPUT_PINS_D 0xF300

#define DIGIT1_GND 0x200 //PTA
#define DIGIT2_GND 0x40 //PTA
#define DIGIT3_GND 0x800 //PTA
#define DIGIT4_GND 0x80 //PTA

#define DISPLAY_DP 0x4000 //PTD
#define DISPLAY_SEGMENT_A 0x1000 //PTD
#define DISPLAY_SEGMENT_B 0x100//PTD
#define DISPLAY_SEGMENT_C 0x100 //PTA
#define DISPLAY_SEGMENT_D 0x8000 //PTD
#define DISPLAY_SEGMENT_E 0x2000 //PTD
#define DISPLAY_SEGMENT_F 0x200 //PTD
#define DISPLAY_SEGMENT_G 0x400 //PTA

// Hodnota za kterou se m� znovu prov�st m��en� vzd�lenosti
#define MEASURE_AFTER 0x80

// Prom�nn� pro uchov�n� aktu�ln� nam��enn� vzd�lenosti
char digit_1 = ' ';
char digit_2 = ' ';
char digit_3 = '0';
char digit_4 = '0';
int dot = 3; // Posledn� pozice bude v�dy pro mm

// Prom�nn� pro pr�m�rov�n� m��en�
#define BUFFER_LEN 10
int actual_position = 0;
double buffer[BUFFER_LEN] = {0};

//#################### Funkce ###########################

// Funkce pro aktivn� �ek�n�
void Delay(uint64_t bound)
{
	for (uint64_t i = 0; i < bound; i++)
	{
		__NOP();
	}
}

// Vysl�n� ultrazvukov�ho pulzu ze senzoru
void Send_Burst()
{
	PTA->PSOR |= TRIG_PIN;

	// Po�k�n� minim�ln� 10uS
	// pou�ito kolem 50uS
	Delay(200);

	PTA->PCOR |= TRIG_PIN;
}

// V�pis hodnoty na zadan� digit
void Print_Digit(int digit)
{
	// Spu�t�n� aktu�ln�ho digit -> nastaven� GND
	// A z�sk�n� ��la k vyps�n� podle aktu�ln�ho digitu

	// Vynulov�n� v�ech hodnot p�ed nastaven�m v�stup�
	PTA->PSOR |= (DIGIT1_GND | DIGIT2_GND | DIGIT3_GND | DIGIT4_GND);

	PTD->PCOR |= (DISPLAY_DP | DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_B | DISPLAY_SEGMENT_D | DISPLAY_SEGMENT_E | DISPLAY_SEGMENT_F);
	PTA->PCOR |= (DISPLAY_SEGMENT_C | DISPLAY_SEGMENT_G);

	// Vybr�n� ��sla keter� se m� zobrazit
	char number;
	switch(digit)
	    {
	        case 1:
	        	PTA->PCOR |= DIGIT1_GND;

	        	number = digit_1;
	            break;

	        case 2:
	        	PTA->PCOR |= DIGIT2_GND;

	        	number = digit_2;
	            break;

	        case 3:
	        	PTA->PCOR |= DIGIT3_GND;

	        	number = digit_3;
	            break;

	        case 4:
	        	PTA->PCOR |= DIGIT4_GND;

	        	number = digit_4;
	            break;

	        default:
	            // Chyba
	        	return;
	    }

		/*
		 * Segmenty pot�ebn� pro zhotoven� ��sla
		 * 0 - A,B,C,D,E,F
		 * 1 - B,C
		 * 2 - A,B,D,E,G
		 * 3 - A,B,C,D,G
		 * 4 - B,C,F,G
		 * 5 - A,C,D,F,G
		 * 6 - A,C,D,E,F,G
		 * 7 - A,B,C
		 * 8 - A,B,C,D,E,F,G
		 * 9 - A,B,C,D,F,G
		 */

		// Spu�t�n� p��slu�n�ch segment�
		switch(number)
		{
			case '0':
				PTD->PSOR |= (DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_B | DISPLAY_SEGMENT_D | DISPLAY_SEGMENT_E | DISPLAY_SEGMENT_F);
				PTA->PSOR |= DISPLAY_SEGMENT_C;

			    break;

			case '1':
				PTD->PSOR |= DISPLAY_SEGMENT_B;
				PTA->PSOR |= DISPLAY_SEGMENT_C;

			    break;

			case '2':
				PTD->PSOR |= (DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_B | DISPLAY_SEGMENT_D | DISPLAY_SEGMENT_E);
				PTA->PSOR |= DISPLAY_SEGMENT_G;

			    break;

			case '3':
				PTD->PSOR |= (DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_B | DISPLAY_SEGMENT_D);
				PTA->PSOR |= (DISPLAY_SEGMENT_C | DISPLAY_SEGMENT_G);
			    break;

			case '4':
				PTD->PSOR |= (DISPLAY_SEGMENT_B | DISPLAY_SEGMENT_F);
				PTA->PSOR |= (DISPLAY_SEGMENT_C | DISPLAY_SEGMENT_G);

			    break;

			case '5':
				PTD->PSOR |= (DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_D | DISPLAY_SEGMENT_F);
				PTA->PSOR |= (DISPLAY_SEGMENT_C | DISPLAY_SEGMENT_G);

			    break;

			case '6':
				PTD->PSOR |= (DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_D | DISPLAY_SEGMENT_E | DISPLAY_SEGMENT_F);
				PTA->PSOR |= (DISPLAY_SEGMENT_C | DISPLAY_SEGMENT_G);

			    break;

			case '7':
				PTD->PSOR |= (DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_B);
				PTA->PSOR |= DISPLAY_SEGMENT_C;

			    break;

			case '8':
				PTD->PSOR |= (DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_B | DISPLAY_SEGMENT_D | DISPLAY_SEGMENT_E | DISPLAY_SEGMENT_F);
				PTA->PSOR |= (DISPLAY_SEGMENT_C | DISPLAY_SEGMENT_G);
			    break;

			case '9':
				PTD->PSOR |= (DISPLAY_SEGMENT_A | DISPLAY_SEGMENT_B | DISPLAY_SEGMENT_D | DISPLAY_SEGMENT_F);
				PTA->PSOR |= (DISPLAY_SEGMENT_C | DISPLAY_SEGMENT_G);

			    break;

			default:
			    // Chyba
			    return;
		}

		// Zobrazen� desetinn� te�ky pokud na t�to pozici m� b�t
		if (digit == dot)
		{
			PTD->PSOR |= DISPLAY_DP;
		}
}

// Inicializace MCU - zakladni nastaveni hodin, vypnuti watchdogu
void MCU_Init(void)
{
	MCG_C4 |= (MCG_C4_DMX32_MASK | MCG_C4_DRST_DRS(0x01));
	SIM_CLKDIV1 |= SIM_CLKDIV1_OUTDIV1(0x00);
	WDOG_STCTRLH &= ~WDOG_STCTRLH_WDOGEN_MASK;

	// Nastaven� extern�ch hodin, budou vyu�ity LPTMR na m��en� vzd�lenosti
	OSC_CR = (OSC_CR_ERCLKEN(1) | OSC_CR_EREFSTEN(1));
}

// Inicializace pin� pro ultrazvukov� m��i� a segmentov� display
void Pin_Init(void)
{
	// Zprovozn�ni Hodin na PTA a PTD
	SIM->SCGC5 = SIM_SCGC5_PORTA_MASK | SIM_SCGC5_PORTD_MASK;

	// Nastaven� pin� na spr�vnou roli
	// Display
	PORTD->PCR[8] = PORT_PCR_MUX(0x01); // 1
	PORTD->PCR[9] = PORT_PCR_MUX(0x01); // 2
	PORTD->PCR[12] = PORT_PCR_MUX(0x01); // 3
	PORTD->PCR[13] = PORT_PCR_MUX(0x01); // 4
	PORTD->PCR[15] = PORT_PCR_MUX(0x01); // 5
	PORTD->PCR[14] = PORT_PCR_MUX(0x01); // 6
	PORTA->PCR[8] = PORT_PCR_MUX(0x01); // 7
	PORTA->PCR[10] = PORT_PCR_MUX(0x01); // 8
	PORTA->PCR[6] = PORT_PCR_MUX(0x01); // 9
	PORTA->PCR[11] = PORT_PCR_MUX(0x01); // 10
	PORTA->PCR[7] = PORT_PCR_MUX(0x01); // 11
	PORTA->PCR[9] = PORT_PCR_MUX(0x01); // 12

	//Senzor
	PORTA->PCR[26] = PORT_PCR_MUX(0x01); // Trig
	PORTA->PCR[27] = ( PORT_PCR_ISF(0x01) | PORT_PCR_IRQC(0x0B) | PORT_PCR_PFE(0x01) | PORT_PCR_MUX(0x01) | PORT_PCR_PE(0x01) | PORT_PCR_PS(0x00)); // Echo pin, nastaven� interuptu kdy� je na pin p�ivedena 0

	// Nastaven� p��slu�n�ch pin� na v�stupn�
	PTD->PDDR = GPIO_PDDR_PDD(OUTPUT_PINS_D);
	PTA->PDDR = GPIO_PDDR_PDD(OUTPUT_PINS_A); // jen 27 je vstup

	// Inicializa�n� hodnota pro uzem�uj�c� piny displeje
	// Nastaven� na logickou 1 aby byly vypnuty
	PTA->PSOR |= (DIGIT1_GND | DIGIT2_GND | DIGIT3_GND | DIGIT4_GND);

	// Smaz�n� existuj�c�ch p�eru�en� a poloven� p�eru�en� na PORTA
	NVIC_ClearPendingIRQ(PORTA_IRQn);
	NVIC_EnableIRQ(PORTA_IRQn);
}

// Inicializace Low Power Timeru
// Nastaven� je ur�eno pro zm��en� �asu ne� doraz� odra�en� ultrazvukov� pulz
void LPTMR0_Init_BurstTime()
{
    SIM_SCGC5 |= SIM_SCGC5_LPTIMER_MASK; // Zapnut� hodin

    // Vypnut� timeru aby ho �lo nastavit
    LPTMR0_CSR &= ~LPTMR_CSR_TEN_MASK;

    // Nastaven� hodin na extern� zdroj o 50Mhz a vyd�len� 32
    LPTMR0_PSR = (LPTMR_PSR_PRESCALE(4) | LPTMR_PSR_PCS(3));

    // Nastaven� po��t�n� do maxima 16bit
    LPTMR0_CMR = 0xFFFF;

    // Smazan� u� existuj�c�ch p�eru�en�
    LPTMR0_CSR = (LPTMR_CSR_TCF_MASK);

    // Zapnut� timeru
    LPTMR0_CSR |= LPTMR_CSR_TEN_MASK;
}

// Inicializace Low Power Timeru
// Nastaven� je ur�eno pro aktivov�n� m��en� nov� vzd�lenosti
void LPTMR0_Init_Delay(int count)
{
    SIM_SCGC5 |= SIM_SCGC5_LPTIMER_MASK; // Zapnut� hodin

    // Vypnut� timeru aby ho �lo nastavit
    LPTMR0_CSR &= ~LPTMR_CSR_TEN_MASK;

    // Nastaven� hodin 1Khz
    LPTMR0_PSR = (LPTMR_PSR_PRESCALE(0) | LPTMR_PSR_PBYP_MASK | LPTMR_PSR_PCS(1)) ;

    // Nastaven� hodnoty do kter� se m� po��tat
    LPTMR0_CMR = count;

    // Smazan� u� existuj�c�ch p�eru�en� a zapnut� p�eru�en�
    LPTMR0_CSR = (LPTMR_CSR_TCF_MASK | LPTMR_CSR_TIE_MASK);

    // Povolen� p�eru�en� od timeru LPTMR0
    NVIC_EnableIRQ(LPTMR0_IRQn);

    // Zapnut� timeru
    LPTMR0_CSR |= LPTMR_CSR_TEN_MASK;
}

// Zpracov�n� p�eru�en� od timeru, kter� indikuje �e se m� zm��it vzd�lenost
void LPTMR0_IRQHandler(void)
{
	// Vypnut� timeru
	LPTMR0_CSR &= ~LPTMR_CSR_TEN_MASK;

    // Posl�n� ultrazvukov�ho pulzu
    Send_Burst();
}

// P�eru�en� od echo pinu
void PORTA_IRQHandler(void)
{
	// Na pinu je logicka 1
	// Zapnut� timeru pro zm��en� �asu ne� doraz� odraz
	if ((PORTA->ISFR & ECHO_PIN) && (PTA->PDIR & ECHO_PIN))
	{
		PORTA_ISFR = ECHO_PIN;
		LPTMR0_Init_BurstTime();

		return;
	}

	// Na pinu je logicka 0
	// Vypnut� timeru a ur�en� vzd�lenosti objektu podle �asu
	if ((PORTA->ISFR & ECHO_PIN) && !(PTA->PDIR & ECHO_PIN))
	{
		// z�sk�n� cykl� timeru ne� ultrazvukov� sign�l dorazil zp�t
		LPTMR0->CNR = 0U;
		uint32_t cicles = (uint32_t)((LPTMR0->CNR & LPTMR_CNR_COUNTER_MASK) >> LPTMR_CNR_COUNTER_SHIFT);
		LPTMR0_CSR &= ~LPTMR_CSR_TEN_MASK;

		// Vypo��t�n� vzd�lenosti v cm
		// Jeden tik p�obli�n� 640 ns, 50Mhz/32 = 1,5625 Mhz
		// Rychlost ultrazvuku 340m/s
		double time_in_us = cicles * 0.64;
		double distance_in_um = time_in_us * 340 /2;
		double distance_in_cm = distance_in_um / 10000.0;

		// Pro konzistentn� hodnotu se pr�m�ruje 10 posledn�ch hodnot
		buffer[actual_position] = distance_in_cm;
	    actual_position = (actual_position+1) % BUFFER_LEN;

	    // Zpr�m�rov�n� posledn�ch hodnot. P�i za��tku m��en� se pr�zdn� hodnoty p�eskakuj�
		double average_distance_in_cm = 0;
		int skipped = 0;
		for(int i = 0; i < BUFFER_LEN; i++)
		{
			if (buffer[i] != 0.0)
			{
				average_distance_in_cm += buffer[i];
			}
			else
			{
				skipped += 1;
			}
		}
		average_distance_in_cm /= (BUFFER_LEN - skipped);

		// Zobrazen� hodnoty na display
		// p�eveden� na mm a zaokrouhlen�
		uint32_t average_distance_in_mm = average_distance_in_cm * 10;
		for(int i = 0; i < 4; i++)
		{
			switch (i)
			{
				case 0:
					digit_4 = (average_distance_in_mm % 10) + '0';
					break;
				case 1:
					digit_3 = (average_distance_in_mm % 10) + '0';
					break;
				case 2:
					digit_2 = (average_distance_in_mm % 10) + '0';
					break;
				case 3:
					digit_1 = (average_distance_in_mm % 10) + '0';
					break;
			}

			average_distance_in_mm /= 10;
		}

		PORTA_ISFR = ECHO_PIN;

		// Spu�t�n� norm�ln�ho b�hu timeru pro vyvol�n� dal�� me�en� vzd�lenosti
		LPTMR0_Init_Delay(MEASURE_AFTER);

		return;
	}
}

int main(void)
{

	MCU_Init();		// Zakladni inicializace vlastniho MCU - hodiny, watchdog
	Pin_Init();		// Inicializace pot�ebn�ch vstup� a v�stup�
	LPTMR0_Init_Delay(MEASURE_AFTER);		// Inicializace timeru, kter� vylov� m��en� vzd�lenosti

    // Nekone�n� smy�ka pro v�pis na display
	int actual_digit = 4;
    while (1)
    {
    	Print_Digit(actual_digit);

    	// Lehk� zpo�d�n� mezi zobrazen� ��slic, kolem 250 uS
    	Delay(1000);

    	// P�ehozen� na dal�� segment
    	actual_digit -= 1;
    	if (actual_digit == 0)
    	{
    		actual_digit = 4;
    	}
    }

    return 0;
}

// END main.c
